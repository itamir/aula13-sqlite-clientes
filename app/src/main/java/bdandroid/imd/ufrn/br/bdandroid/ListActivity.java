package bdandroid.imd.ufrn.br.bdandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import adapters.ClienteAdapter;
import daos.ClienteDao;
import dominio.Cliente;


public class ListActivity extends Activity {

    private ListView listClientes;
    private ClienteAdapter adapter;
    private Cliente clienteRemover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ClienteDao clienteDao = new ClienteDao(this);
        List<Cliente> clientes = clienteDao.listar();

        listClientes = (ListView) findViewById(R.id.listClientes);
        adapter = new ClienteAdapter(this, R.layout.row_cliente, clientes);
        listClientes.setAdapter(adapter);
        listClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                clienteRemover = (Cliente) adapterView.getItemAtPosition(i);
                confirmarRemocaoDialog();
            }
        });

    }

    public void confirmarRemocaoDialog() {
        AlertDialog.Builder alerta = new AlertDialog.Builder(this);
        alerta.setTitle("Aten\u00e7\u00e3o");
        alerta.setMessage("Deseja realmente remover?");
        alerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClienteDao clienteDao = new ClienteDao(ListActivity.this);
                clienteDao.remover(clienteRemover);
                adapter.remove(clienteRemover);
            }
        });
        alerta.setNegativeButton("N\u00e3o", null);
        alerta.show();
    }
}
