package bdandroid.imd.ufrn.br.bdandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import daos.ClienteDao;
import dominio.Cliente;
import helpers.ClienteDbHelper;


public class MainActivity extends Activity {


    private EditText nomeTxt, cpfTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nomeTxt = (EditText) findViewById(R.id.nomeTxt);
        cpfTxt = (EditText) findViewById(R.id.cpfTxt);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_listar) {
            Intent i = new Intent(MainActivity.this, ListActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    public void cadastrar(View view) {
        if(!nomeTxt.getText().toString().isEmpty()) {
            Cliente cliente = new Cliente();
            cliente.setNome(nomeTxt.getText().toString());
            cliente.setCpf(cpfTxt.getText().toString());
            ClienteDao clienteDao = new ClienteDao(this);
            clienteDao.inserirOuAtualizar(cliente);

            AlertDialog.Builder alerta = new AlertDialog.Builder(this);
            alerta.setTitle("Aten\u00e7\u00e3o");
            alerta.setMessage("Cliente cadastrado com sucesso!");
            alerta.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    nomeTxt.setText("");
                    cpfTxt.setText("");
                }
            });
            alerta.show();
        } else {
            nomeTxt.setError("Nome: campo obrigat\u00f3rio.");
        }
    }



}
