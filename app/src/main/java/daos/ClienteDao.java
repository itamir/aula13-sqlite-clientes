package daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import dominio.Cliente;
import helpers.ClienteDbHelper;

/**
 * Created by itamir.filho on 19/05/2015.
 */
public class ClienteDao {

    private SQLiteDatabase bd;

    public ClienteDao(Context contexto) {
        ClienteDbHelper clienteDbHelper = new ClienteDbHelper(contexto);
        bd = clienteDbHelper.getWritableDatabase();
    }

    public void inserirOuAtualizar(Cliente c) {
        ContentValues valores=new ContentValues(2);
        valores.put("NOME", c.getNome());
        valores.put("CPF", c.getCpf());
        if(c.getId() > 0) {
            valores.put("CPF", c.getCpf());
            bd.update("CLIENTE", valores, "id = ?",
                    new String[] { "" + c.getId()});
        } else {
            bd.insert("CLIENTE", null, valores);
        }
    }
    public void remover(Cliente c) {
        String[] id = {String.valueOf(c.getId())};
        bd.delete("CLIENTE", "ID=?", id);
    }
    public List<Cliente> listar(){
        List<Cliente> clientes = new ArrayList<Cliente>();
        Cursor c = bd.query("CLIENTE", Cliente.COLUNAS,
                null, null, null, null, "NOME");
        if (c.moveToFirst()){
            do{
                Cliente cliente = new Cliente();
                cliente.setId(c.getInt(0));
                cliente.setNome(c.getString(1));
                cliente.setCpf(c.getString(2));
                clientes.add(cliente);
            }while(c.moveToNext());
        }
        c.close();
        return clientes;
    }

    public Cliente buscarPorChavePrimaria(int id){
        Cliente cliente = new Cliente();

        Cursor cursor = bd.query("CLIENTE", Cliente.COLUNAS,
                "id="+id, null, null, null, null);

        if (cursor.moveToFirst()){
            cliente.setId(cursor.getInt(0));
            cliente.setNome(cursor.getString(1));
            cliente.setCpf(cursor.getString(2));
        }
        cursor.close();
        return cliente;
    }

}


