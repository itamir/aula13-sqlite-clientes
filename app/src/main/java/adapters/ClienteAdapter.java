package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import bdandroid.imd.ufrn.br.bdandroid.R;
import dominio.Cliente;

/**
 * Created by itamir.filho on 19/05/2015.
 */
public class ClienteAdapter extends ArrayAdapter<Cliente> {

    static class ViewHolder{
        TextView nomeTxt;
        TextView cpfText;
    }

    public ClienteAdapter(Context context, int
            textViewResourceId,List<Cliente> objects) {
        super(context, textViewResourceId, objects);
    }

    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder = null;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_cliente, null);
            holder = new ViewHolder();

            holder.nomeTxt =  (TextView) view.findViewById(R.id.nomeRowText);
            holder.cpfText =  (TextView) view.findViewById(R.id.cpfRowText);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Cliente cliente = getItem(position);
        if (cliente!= null) {
            holder.nomeTxt.setText(cliente.getNome());
            holder.cpfText.setText(cliente.getCpf());
        }
        return view;
    }

}

