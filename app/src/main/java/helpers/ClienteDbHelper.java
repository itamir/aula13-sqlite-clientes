package helpers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by itamir.filho on 19/05/2015.
 */
public class ClienteDbHelper extends SQLiteOpenHelper {

    public static final String NOME_BANCO = "clientes";
    public static final int VERSAO_BANCO = 1;
    public static final String CLIENTE_TABLE = "CREATE TABLE CLIENTE ( " +
            "ID INTEGER NOT NULL PRIMARY KEY autoincrement, " +
            "NOME TEXT, " +
            "CPF TEXT);";

    public ClienteDbHelper(Context context) {
        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CLIENTE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS CLIENTE");
        onCreate(sqLiteDatabase);
    }


}
