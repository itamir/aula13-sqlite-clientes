package dominio;

import java.io.Serializable;

/**
 * Created by itamir.filho on 19/05/2015.
 */
public class Cliente implements Serializable{

    public static String[] COLUNAS = new String[]{"ID", "NOME", "CPF"};

    private int id;

    private String nome;

    private String cpf;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
